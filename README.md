# williecadete-packer-centos7

Packer templates for building base VM boxes for Virtualbox.

# Usage

## Installing Packer

To install packer, first find the appropriate package for your system and download it.

    https://www.packer.io/downloads.html

If you're using Homebrew

    $ brew tap homebrew/binary
    $ brew install packer

## Running Packer

    $ git clone https://github.com/williecadete/packer-centos7.git
    $ cd packer-centos7
    $ packer build packer.json

## Supported versions

This templates was tested using a packer 0.10.2.
