#!/bin/bash

echo "installing syadmin packages"
yum install -y net-tools git vim-enhanced wget rsync bind-utils nc telnet \
unzip tcpdump man mlocate ccze htop traceroute epel-release sysstat wget \
screen git
yum update -y

echo "export LC_ALL=en_US.UTF-8" > /etc/profile.d/custom_locale.sh
chmod 755 /etc/profile.d/custom_locale.sh
source /etc/profile.d/custom_locale.sh
