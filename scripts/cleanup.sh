#!/bin/sh

echo "Remove Linux headers"
yum -y remove gcc cpp kernel-devel kernel-headers
yum -y clean all

echo "Cleanup Cache files"
rm -rf /var/cache/yum

echo "Remove Virtualbox specific files"
rm -rf /usr/src/vboxguest* /usr/src/virtualbox-ose-guest*
rm -rf *.iso *.iso.? /tmp/vbox /home/vagrant/.vbox_version

echo "Cleanup log files"
find /var/log -type f | while read f; do echo -ne '' > $f; done;

echo "remove under tmp directory"
rm -rf /tmp/*

echo "remove interface persistent"
rm -f /etc/udev/rules.d/70-persistent-net.rules

echo "Better compression file"
dd if=/dev/zero of=/EMPTY bs=1M
rm -rf /EMPTY
